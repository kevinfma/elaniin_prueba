package android.pruebaelaniin.kevinhernandez.com.elaniin_prueba.models;

/**
 * Created by kevinFma on 3/25/2018.
 *
 * Class used for getting elements that uses the same format (url and name)
 */

public class GenericClass {
    private String url;
    private String name;

    public GenericClass(String url, String name){
        this.url = url;
        this.name = name;
    }

    public GenericClass(){}

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
