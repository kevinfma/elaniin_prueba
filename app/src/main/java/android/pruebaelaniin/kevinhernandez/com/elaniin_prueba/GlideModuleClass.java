package android.pruebaelaniin.kevinhernandez.com.elaniin_prueba;

import com.bumptech.glide.annotation.GlideModule;
import com.bumptech.glide.module.AppGlideModule;

/**
 * Created by kevinFma on 3/24/2018.
 *
 * Class used for implementing the GlideApp class for the image management through all the windows
 * in the app
 */

@GlideModule
public class GlideModuleClass extends AppGlideModule {
}
