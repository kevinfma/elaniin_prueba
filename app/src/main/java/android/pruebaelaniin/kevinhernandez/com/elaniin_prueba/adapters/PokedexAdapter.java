package android.pruebaelaniin.kevinhernandez.com.elaniin_prueba.adapters;

import android.content.Context;
import android.pruebaelaniin.kevinhernandez.com.elaniin_prueba.R;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

/**
 * Created by kevinFma on 3/25/2018.
 *
 * Adapter using for loading all the pokedexes that belongs to a specific region, this is
 * implemented as a RecyclerView adapter and showed in the create team window
 */

public class PokedexAdapter extends RecyclerView.Adapter<PokedexAdapter.ViewHolder> {

    private List<String> pokedexes;
    private int layout;
    private onItemClickListener itemClickListener;
    private Context context;

    public PokedexAdapter(List<String> pokedexes, int layout, onItemClickListener listener){
        this.pokedexes = pokedexes;
        this.layout = layout;
        this.itemClickListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(layout, parent, false);
        context = parent.getContext();
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bind(pokedexes.get(position), itemClickListener);
    }

    @Override
    public int getItemCount() { return pokedexes.size(); }

    public class ViewHolder extends RecyclerView.ViewHolder{

        private TextView textViewName;

        public ViewHolder(View itemView) {
            super(itemView);
            textViewName = itemView.findViewById(R.id.textViewPokedex);
        }

        public void bind(final String name, final onItemClickListener listener){
            textViewName.setText(name);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onItemClick(name, getAdapterPosition(),itemView);
                }
            });
        }
    }

    public interface onItemClickListener{
        void onItemClick(String name, int position, View view);
    }
}
