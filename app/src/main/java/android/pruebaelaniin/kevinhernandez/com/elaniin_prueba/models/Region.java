package android.pruebaelaniin.kevinhernandez.com.elaniin_prueba.models;

import android.pruebaelaniin.kevinhernandez.com.elaniin_prueba.models.GenericClass;

import java.util.List;

/**
 * Created by kevinFma on 3/25/2018.
 *
 * Class representing a region for getting the pokedexes in the PokedexActivity
 */

public class Region {
    private int id;
    private String name;
    private List<GenericClass> pokedexes;

    public Region(int id, String name, List<GenericClass> pokedexes){
        this.setId(id);
        this.setName(name);
        this.setPokedexes(pokedexes);
    }

    public Region(){}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<GenericClass> getPokedexes() {
        return pokedexes;
    }

    public void setPokedexes(List<GenericClass> pokedexes) {
        this.pokedexes = pokedexes;
    }
}
