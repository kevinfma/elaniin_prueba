package android.pruebaelaniin.kevinhernandez.com.elaniin_prueba.models;

import java.util.List;

/**
 * Created by kevinFma on 3/25/2018.
 *
 * Class representing the team table from Database
 * Relations: userId   -> the team owner
 *            regionId -> the region to which the pokemons belong
 */

public class Team {
    private String id;
    private String name;
    private int regionId;
    private String userId;
    private int slots_used;
    private List<Slots> slots;
    private boolean isPublic;
    private String code;

    public Team(String id, String name, int regionId, String userId, int slots_used, List<Slots> slots,
                boolean isPublic, String code){
        this.setId(id);
        this.setName(name);
        this.setRegionId(regionId);
        this.setUserId(userId);
        this.setSlots_used(slots_used);
        this.setSlots(slots);
        this.setPublic(isPublic);
        this.setCode(code);
    }

    public Team(){}

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getRegionId() {
        return regionId;
    }

    public void setRegionId(int regionId) {
        this.regionId = regionId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public int getSlots_used() {
        return slots_used;
    }

    public void setSlots_used(int slots_used) {
        this.slots_used = slots_used;
    }

    public List<Slots> getSlots() {
        return slots;
    }

    public void setSlots(List<Slots> slots) {
        this.slots = slots;
    }

    public boolean isPublic() {
        return isPublic;
    }

    public void setPublic(boolean aPublic) {
        isPublic = aPublic;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
