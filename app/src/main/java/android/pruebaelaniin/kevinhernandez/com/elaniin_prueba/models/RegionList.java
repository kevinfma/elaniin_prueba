package android.pruebaelaniin.kevinhernandez.com.elaniin_prueba.models;

import android.pruebaelaniin.kevinhernandez.com.elaniin_prueba.models.GenericClass;

import java.util.List;

/**
 * Created by kevinFma on 3/24/2018.
 *
 * Class used for getting all the regions in the MainActivity
 */

public class RegionList {
    private int count;
    private String previous;
    private List<GenericClass> results;
    private String next;


    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getPrevious() {
        return previous;
    }

    public void setPrevious(String previous) {
        this.previous = previous;
    }

    public List<GenericClass> getResults() {
        return results;
    }

    public void setResults(List<GenericClass> results) {
        this.results = results;
    }

    public String getNext() {
        return next;
    }

    public void setNext(String next) {
        this.next = next;
    }
}
