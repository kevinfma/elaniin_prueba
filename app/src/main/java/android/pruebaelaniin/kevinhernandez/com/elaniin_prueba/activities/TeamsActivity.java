package android.pruebaelaniin.kevinhernandez.com.elaniin_prueba.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.pruebaelaniin.kevinhernandez.com.elaniin_prueba.GlideApp;
import android.pruebaelaniin.kevinhernandez.com.elaniin_prueba.MySingleton;
import android.pruebaelaniin.kevinhernandez.com.elaniin_prueba.R;
import android.pruebaelaniin.kevinhernandez.com.elaniin_prueba.adapters.TeamsAdapter;
import android.pruebaelaniin.kevinhernandez.com.elaniin_prueba.models.FlavorTextEntries;
import android.pruebaelaniin.kevinhernandez.com.elaniin_prueba.models.Pokemon;
import android.pruebaelaniin.kevinhernandez.com.elaniin_prueba.models.Region;
import android.pruebaelaniin.kevinhernandez.com.elaniin_prueba.models.Slots;
import android.pruebaelaniin.kevinhernandez.com.elaniin_prueba.models.Species;
import android.pruebaelaniin.kevinhernandez.com.elaniin_prueba.models.Team;
import android.pruebaelaniin.kevinhernandez.com.elaniin_prueba.models.Type;
import android.pruebaelaniin.kevinhernandez.com.elaniin_prueba.models.Types;
import android.pruebaelaniin.kevinhernandez.com.elaniin_prueba.models.User;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class TeamsActivity extends AppCompatActivity {

    //      View items      //
    private ImageView mImageViewAvatar, mImageViewMenu;
    private RecyclerView mRecyclerViewTeams;
    private TextView mTextViewUser;

    //      Logic items     //
    private User mLoggedUser;
    private List<Team> mTeamList, mTeamListComplete;
    private List<Region> mRegionList;
    private DatabaseReference mDatabasePokemon, mDatabaseUser, mDatabaseRegion;
    private FirebaseAuth mFirebaseAuth;
    private RecyclerView.Adapter mAdapterTeams;
    private static final String SERVER_URL = "https://pokeapi.co/api/v2/";
    private static final String POKEMON_URL = SERVER_URL + "pokemon/";
    private static final String SPECIES_URL = SERVER_URL + "pokemon-species/";
    private boolean mReadDB = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teams);

        //                              nitializations
        try{
            Bundle bundle = getIntent().getExtras();
            mLoggedUser = new Gson().fromJson(bundle != null ? bundle.getString("user") : null,User.class);

            Typeface mCustomFont = Typeface.createFromAsset(getAssets(), "fonts/ostrich-regular.ttf");
            mImageViewAvatar = findViewById(R.id.imageViewAvatar);
            mImageViewMenu = findViewById(R.id.imageViewMenu);
            mTextViewUser = findViewById(R.id.textViewUser);
            mTextViewUser.setTypeface(mCustomFont);
            mTextViewUser.setText(mLoggedUser.getUsername());
            mRecyclerViewTeams = findViewById(R.id.recyclerViewTeams);


            mTeamList = new ArrayList<>();
            mTeamListComplete = new ArrayList<>();
            mRegionList = new ArrayList<>();
            mDatabasePokemon = FirebaseDatabase.getInstance().getReference("team");
            mDatabaseUser = FirebaseDatabase.getInstance().getReference("user");
            mDatabaseRegion = FirebaseDatabase.getInstance().getReference("region");
            mFirebaseAuth = FirebaseAuth.getInstance();

            createRecyclerView();

            if(mLoggedUser !=null) setUserParams(mLoggedUser);
            loadTeams();
            loadRegions();
        }catch (Exception e){
            Toast.makeText(this, "Error en onCreate: " + e.getClass(), Toast.LENGTH_SHORT).show();
        }
    }

    //                                  User created events

    /**
     * Creates the RecyclerView object for the list of teams displayed, uses the TeamsAdapter
     * Adapter for binding the views
     */
    private void createRecyclerView() {
        try{
            mRecyclerViewTeams.setHasFixedSize(true);

            RecyclerView.LayoutManager layoutManagerTeams = new LinearLayoutManager(this);

            mRecyclerViewTeams.setLayoutManager(layoutManagerTeams);

            ArrayList<TeamsAdapter.onItemClickListener> listeners = new ArrayList<>();
            //  pkmn1
            listeners.add(new TeamsAdapter.onItemClickListener() {
                @Override
                public void onItemClick(Slots slot, int position, Team team) {
                    getPokemonDetails(slot);
                }
            });
            //  pkmn2
            listeners.add(new TeamsAdapter.onItemClickListener() {
                @Override
                public void onItemClick(Slots slot, int position, Team team) {
                    getPokemonDetails(slot);
                }
            });
            //  pkmn3
            listeners.add(new TeamsAdapter.onItemClickListener() {
                @Override
                public void onItemClick(Slots slot, int position, Team team) {
                    getPokemonDetails(slot);
                }
            });
            //  pkmn4
            listeners.add(new TeamsAdapter.onItemClickListener() {
                @Override
                public void onItemClick(Slots slot, int position, Team team) {
                    getPokemonDetails(slot);
                }
            });
            //  pkmn5
            listeners.add(new TeamsAdapter.onItemClickListener() {
                @Override
                public void onItemClick(Slots slot, int position, Team team) {
                    getPokemonDetails(slot);
                }
            });
            //  pkmn6
            listeners.add(new TeamsAdapter.onItemClickListener() {
                @Override
                public void onItemClick(Slots slot, int position, Team team) {
                    getPokemonDetails(slot);
                }
            });
            //  Delete button
            listeners.add(new TeamsAdapter.onItemClickListener() {
                @Override
                public void onItemClick(@Nullable Slots slot, int position, final Team team) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(TeamsActivity.this);
                    builder.setTitle("Do you wish to delete team "+team.getName()+"?");
                    builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            removeTeam(team);
                        }
                    });
                    builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                        }
                    });
                    builder.show();
                }
            });
            //  Change privacy for team
            listeners.add(new TeamsAdapter.onItemClickListener() {
                @Override
                public void onItemClick(@Nullable Slots slot, int position, final Team team) {
                    String state = (!team.isPublic()) ? "public" : "private";
                    AlertDialog.Builder builder = new AlertDialog.Builder(TeamsActivity.this);
                    builder.setTitle("Do you wish to put this team as " + state + "?");
                    builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            updatePrivateTeam(team);
                        }
                    });
                    builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                        }
                    });
                    builder.show();
                }
            });
            //  Edit team
            listeners.add(new TeamsAdapter.onItemClickListener() {
                @Override
                public void onItemClick(@Nullable Slots slot, int position, Team team) {
                    try{
                        Intent intentPokedex = new Intent(TeamsActivity.this, PokedexActivity.class);
                        final int size = mRegionList.size();
                        Region region = null;
                        for (int i = 0; i < size; i++){
                            if (String.valueOf(team.getRegionId()).equals(String.valueOf(mRegionList.get(i).getId()))){
                                region = mRegionList.get(i);
                                break;
                            }
                        }
                        intentPokedex.putExtra("user", new Gson().toJson(mLoggedUser));
                        intentPokedex.putExtra("region", new Gson().toJson(region));
                        intentPokedex.putExtra("edit", new Gson().toJson(team));
                        startActivity(intentPokedex);
                    }catch (Exception e){
                        Toast.makeText(TeamsActivity.this, "Error en createRecyclerView.new onItemClickListener.onItemClick: " + e.getClass(), Toast.LENGTH_SHORT).show();
                    }
                }
            });

            mAdapterTeams = new TeamsAdapter(mTeamList, R.layout.recycler_item_teams, listeners);

            mRecyclerViewTeams.setAdapter(mAdapterTeams);
        }catch (Exception e){
            Toast.makeText(this, "Error en createRecyclerView: " + e.getClass(), Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Updates the team state (Public or Private) in the Database when the user touches the
     * person/persons icon, if it changes to public, the app displays a message telling the user
     * to use a link generated for copying the team in other user account
     * @param team the target team to update state
     */
    private void updatePrivateTeam(Team team) {
        try{
            for (int i = 0; i < mTeamList.size(); i++){
                if (team.getId().equals(mTeamList.get(i).getId())){
                    final boolean isPublic = !team.isPublic();
                    team.setPublic(isPublic);
                    String code = String.format("%04d", new Random().nextInt(10000));
                    team.setCode(code);
                    mDatabasePokemon.child(team.getId()).setValue(team);
                    mTeamList.get(i).setPublic(isPublic);

                    if (team.isPublic()){
                        AlertDialog.Builder builder = new AlertDialog.Builder(TeamsActivity.this);
                        TextView textView = new TextView(TeamsActivity.this);
                        textView.setText("To copy this team, click the copy button in the top right part in this " +
                                "window and paste this code: " + mLoggedUser.getUsername() + ":" + code);
                        builder.setView(textView);
                        builder.show();
                    }
                    mAdapterTeams.notifyDataSetChanged();
                }
            }
        }catch (Exception e){
            Toast.makeText(this, "Error en updatePrivateTeam: " + e.getClass(), Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Removes a team in the Database when the user touches the red "X" button
     * @param team the target team to be removed
     */
    private void removeTeam(Team team) {
        try{
            for (int i = 0; i < mTeamList.size(); i++){
                if (team.getId().equals(mTeamList.get(i).getId())){
                    mDatabasePokemon.child(team.getId()).removeValue();
                    mTeamList.remove(i);
                    mAdapterTeams.notifyDataSetChanged();
                }
            }
        }catch (Exception e){
            Toast.makeText(this, "Error en removeTeam: " + e.getClass(), Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Displays an AlertDialog with the pokemon number, name, type and description when the user
     * touches a pokemon image
     * @param slots
     */
    private void getPokemonDetails(Slots slots) {
        try{
            AlertDialog.Builder builder = new AlertDialog.Builder(TeamsActivity.this);
            LayoutInflater inflater = this.getLayoutInflater();
            View view = inflater.inflate(R.layout.layout_pokemon_info, null);
            final TextView textViewNumber = view.findViewById(R.id.textViewNumber),
                    textViewName = view.findViewById(R.id.textViewName),
                    textViewType = view.findViewById(R.id.textViewTypes),
                    textViewDescription = view.findViewById(R.id.textViewDescription);
            ImageView imageViewPokemon = view.findViewById(R.id.imageViewPokemon);
            textViewNumber.setText(""+slots.getPokemonNumber());
            GlideApp
                    .with(view.getContext())
                    .load(slots.getUrl())
                    .into(imageViewPokemon);
            textViewName.setText(slots.getName());

            StringRequest request = new StringRequest(Request.Method.GET, POKEMON_URL + slots.getPokemonNumber(),
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try{
                                Pokemon pokemon = new Gson().fromJson(response,Pokemon.class);
                                List<Types> types = pokemon.getTypes();
                                String typesString = "";
                                for (Types item : types){
                                    Type type = item.getType();
                                    typesString = typesString + type.getName() + ",";
                                }
                                typesString = typesString.substring(0, typesString.length() - 1);
                                textViewType.setText(typesString);
                            }catch (Exception e){
                                Toast.makeText(TeamsActivity.this, "Error en getPokemonDetails.new Listener.onResponse: " + e.getClass(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(TeamsActivity.this, "Error getting pokemon types", Toast.LENGTH_SHORT).show();
                }
            });
            MySingleton.getInstance(TeamsActivity.this).addToRequestQueue(request);

            StringRequest request1 = new StringRequest(Request.Method.GET, SPECIES_URL + slots.getPokemonNumber(), new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try{
                        Species species = new Gson().fromJson(response, Species.class);
                        for (FlavorTextEntries item : species.getFlavor_text_entries()){
                            textViewDescription.setText(item.getFlavor_text());
                        }
                    }catch (Exception e){
                        Toast.makeText(TeamsActivity.this, "Error en getPokemonDetails.new Listener.onResponse: " + e.getClass(), Toast.LENGTH_SHORT).show();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(TeamsActivity.this, "Error getting pokemon description " + error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
            request1.setRetryPolicy(new RetryPolicy() {
                @Override
                public int getCurrentTimeout() { return 5000; }

                @Override
                public int getCurrentRetryCount() {
                    return 10;
                }

                @Override
                public void retry(VolleyError error) throws VolleyError {

                }
            });
            MySingleton.getInstance(TeamsActivity.this).addToRequestQueue(request1);

            builder.setView(view);
            AlertDialog dialog = builder.create();
            dialog.show();
        }catch (Exception e){
            Log.d("DETAILS", "getPokemonDetails: "+e.getMessage());
            Toast.makeText(this, "Error en getPokemonDetails: " + e.getClass(), Toast.LENGTH_SHORT).show();
        }

    }

    /**
     * Sets the user image and the user name at the top of the window
     * @param loggedUser the User object passed though the bundle
     */
    private void setUserParams(User loggedUser) {
        try{
            int genre = 0;
            switch (loggedUser.getGenre()){
                case "Male":
                    genre = R.drawable.brendan_mini;
                    break;
                case "Female":
                    genre = R.drawable.may_mini;
                    break;
                default:
                    break;
            }
            GlideApp
                    .with(this)
                    .load(genre)
                    //.fitCenter()
                    .centerCrop()
                    .into(mImageViewAvatar);

            GlideApp
                    .with(TeamsActivity.this)
                    .load(R.mipmap.ic_transfer)
                    .into(mImageViewMenu);

            mImageViewAvatar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try{
                        mFirebaseAuth.signOut();
                        Intent intentLogin = new Intent(TeamsActivity.this, LoginActivity.class);
                        startActivity(intentLogin);
                    }catch (Exception e){
                        Toast.makeText(TeamsActivity.this, "Error en setUserParams.new OnClickListener.onClick: ", Toast.LENGTH_SHORT).show();
                    }
                }
            });

            mImageViewMenu.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    transferTeam();
                }
            });
        }catch (Exception e){
            Toast.makeText(this, "Error en setUserParams: " + e.getClass(), Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Shows an AlertDialog asking for a link, if the code is correct the app copies the inserted
     * link team in the user account
     */
    private void transferTeam() {
        try{
            AlertDialog.Builder builder = new AlertDialog.Builder(TeamsActivity.this);
            builder.setTitle("Copy team");
            final EditText input = new EditText(TeamsActivity.this);
            builder.setView(input);
            builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    try{
                        String c_code = input.getText().toString().trim();
                        String[] parts = c_code.split(":");
                        final String username = parts[0];
                        final String code = parts[1];
                        mDatabaseUser.addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                try{
                                    for (DataSnapshot item : dataSnapshot.getChildren()){
                                        User user = item.getValue(User.class);
                                        if (user != null && user.getUsername().equals(username)){
                                            for (int i = 0; i < mTeamListComplete.size(); i++){
                                                Team team = mTeamListComplete.get(i);
                                                if (team.getCode().equals(code) && team.isPublic()){
                                                    Team newTeam = team;
                                                    String key = mDatabasePokemon.push().getKey();
                                                    newTeam.setCode(String.format("%04d",new Random().nextInt(10000)));
                                                    newTeam.setId(key);
                                                    newTeam.setPublic(false);
                                                    newTeam.setUserId(mLoggedUser.getId());
                                                    mDatabasePokemon.child(key).setValue(newTeam);
                                                    Intent intentMain = new Intent(TeamsActivity.this, MainActivity.class);
                                                    intentMain.putExtra("user", new Gson().toJson(mLoggedUser));
                                                    Toast.makeText(TeamsActivity.this, "Team copied successfully", Toast.LENGTH_SHORT).show();
                                                    startActivity(intentMain);
                                                }
                                            }
                                        }
                                    }
                                }catch (Exception e){
                                    Toast.makeText(TeamsActivity.this, "Error en transferTeam.new OnClickListener.onClick.new ValueEventListener.onDataChange: " + e.getClass(), Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {
                                Toast.makeText(TeamsActivity.this, "Error in database", Toast.LENGTH_SHORT).show();
                            }
                        });
                    }catch (Exception e){
                        Toast.makeText(TeamsActivity.this, "Error en transferTeam.new OnClickListener.onClick: " + e.getClass(), Toast.LENGTH_SHORT).show();
                    }
                }
            });
            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {

                }
            });
            builder.show();
        }catch (Exception e){
            Toast.makeText(this, "Error en transferTeam: " + e.getClass(), Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Loads all the user teams from the database
     */
    private void loadTeams() {
        mDatabasePokemon.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                try{
                    if (!mReadDB){
                        for (DataSnapshot item : dataSnapshot.getChildren()){
                            Team team = item.getValue(Team.class);
                            mTeamListComplete.add(team);
                            if (team.getUserId().equals(mLoggedUser.getId())){
                                mTeamList.add(team);
                            }
                        }
                        mAdapterTeams.notifyDataSetChanged();
                        mReadDB = true;
                    }
                }catch (Exception e){
                    Toast.makeText(TeamsActivity.this, "Error en loadTeams.new ValueEventListener.onDataChange: " + e.getClass(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Toast.makeText(TeamsActivity.this, "Error en onCancelled", Toast.LENGTH_SHORT).show();
            }
        });
    }

    /**
     * Loads all the regions from the database
     */
    private void loadRegions() {
        mDatabaseRegion.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot item : dataSnapshot.getChildren()){
                    mRegionList.add(item.getValue(Region.class));;
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}