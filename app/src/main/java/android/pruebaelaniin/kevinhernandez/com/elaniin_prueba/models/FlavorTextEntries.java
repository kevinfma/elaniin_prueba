package android.pruebaelaniin.kevinhernandez.com.elaniin_prueba.models;

import java.util.List;

/**
 * Created by kevinFma on 3/26/2018.
 *
 * Class used for getting the pokemon description in the TeamsActivity
 */

public class FlavorTextEntries {
    private String flavor_text;

    public FlavorTextEntries(String flavor_text){
        this.setFlavor_text(flavor_text);
    }

    public FlavorTextEntries(){}

    public String getFlavor_text() {
        return flavor_text;
    }

    public void setFlavor_text(String flavor_text) {
        this.flavor_text = flavor_text;
    }
}
