Propietario: Kevin Hernández
Nombre proyecto: Prueba_elaniin

Descripción: Propyecto desarrollado en Android para la pruba asignada

Funcionamiento:
	-Pantalla de login: El ususario podrá acceder a la aplicación por medio de sus credenciales, a través de Facebook o google, 
	 o podrá ir a la sección de registro; si se realiza el login correctamente lo llevará a la pantalla principal
	-Pantall de registro: El usuario ingresará su información para registrarse en la aplicación, ingresando correo, contraseña, 
	 genero y username, si todo está correcto lo llevará a la pantalla principal
	-Pantalla principal: El usuario seleccionará una región para crear un nuevo equipo desde la lista vertical que aparece en 
	 el centro de la pantalla, podrá cerrar sesión haciendo touch sobre el icono de avatar, ubicado en la parte superior izquierda
	 de la pantalla, o podrá ir a la administración de equipos haciendo touch en el icono de menú ubicado en la parte superior
	 derecha de la pantalla
	-Pantalla de pokedex: El usuario podrá crear nuevos equipos, para ello debe de selccioar un pokedex de los mostrados en la 
	 barra azul debajo de las opciones de usuario (barra verde), una vez seleccionado el pokedex se cargará la información de
	 todos los pokemon que pertenecen a ese pokedex, mostrando una imagen en miniatura de todos estos, luego de haber seleccionado
	 un pokedex se iniciará la creación de equipos haciendo touch en el icono de pokeballs ubicado en la parte superior derecha,
	 este cambiará la informacion de usuarios por una lista de pokeballs en las cuales se almacenarán los pokemon que se 
	 seleccionen; cuando se selecciona un pokemon, se añadira este a la lista de pokeballs, para remover un pokemon de la lista,
	 se debe hacer touch sobre este.
	 Una vez completada la selección de pokemons volvemos a hacer click en el boton de cheque verde en la parte superior derecha,
	 se solicitará un nombre para el equipo, ingresamos el nombre y se guardará el nuevo equipo.
	-Pantall de equipos: El usuario podrá administrar sus equipos, se mostrará la lista de los equipos de forma vertical, 
	 mostrando las imagenes de cada pokemon; para ver el detalle de un pokemon, se debe hacer touch sobre el icono de este;
	 para editar un equipo, se debe hacer touch sobre el icono de papel y lápiz, se redireccionará a la pantalla de pokedex
	 en donde se podra modificar los pokemons de ese equipo; para eliminar un equipo hacemos touch sobre el icono de la "X"
	 roja a la par de el icono de editar; para camibar el estado de un equipo de público a privado y vice-versa, hacemos touch
	 sobre el icono de persona/personas a la par de la "X" de eliminar, cuando un equipo es público, podrá ser copiado por otros
	 usuario utilizando el patron "<username>:<code>", en donde <username> es el nombre del usuario del que deseamos copiar el
	 equipo y <code> es el código auto-generado que se le asigna a cada equipo cuando se crea (este código se muestra en la 
	 pantalla de administración de equipos); para copiar un equipo se debe de hacer touch en el icono de pokeball ubicado en
	 la parte superior derecha de la pantalla, ingresamos el patron como el mencionado anteriormente, aceptamos y se agregará
	 el nuevo equipo a la lista mostrada.
	 
Datos de desarrollo:
	-Versión minima de SDK: 16
	-Versión target de SDK: 26